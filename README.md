# ZYXmeBase

#### 介绍
.net8项目框架，当前版本主要用于技术练习，包含仓储、依赖注入、鉴权等。

更多的是对于新技术和基础框架的学习与探索。

设计目的用于快速搭建软件后台，初始化账号、角色、鉴权认证、仓储等辅助信息。

便于自己后续更专注于开发具体的业务逻辑。

#### 软件架构
.net 8


#### 安装教程

1.  依据实体表创建对应数据库
2.  更改数据库链接即可运行
3.  后期需要提供mysql脚本
4.  更远的后期支持代码一键初始化账号密码角色等更多相关信息
