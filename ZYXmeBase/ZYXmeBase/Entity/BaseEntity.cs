﻿namespace ZYXmeBase.Entity
{
    public abstract class BaseEntity
    {
        public bool IsDeleted { get; set; } // Soft delete flag
        public DateTime CreatedTime { get; set; }
        public string? LastModifierId { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public string CreatorId { get; set; }
        public string? DeleterId { get; set; }

        public BaseEntity()
        {
            CreatedTime = DateTime.Now;
            IsDeleted = false;
        }
    }
}
