﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ZYXmeBase.Entity.User
{

    [Table("db_userinfo")]
    public class UserInfo: BaseEntity
    {
        /// <summary>
        /// 聚合根，主键ID
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// 用户密码Hash
        /// </summary>
        public string PasswordHash { get; set; }
        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 用户电话号码
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 用户活跃状态
        /// </summary>
        public bool IsActive { get; set; }

        // 更多属性...

        public UserInfo()
        {
            Id = Guid.NewGuid().ToString();  // 自动生成聚合根ID
            IsActive = true;   // 默认为活跃状态
            CreatedTime = DateTime.UtcNow;  // 默认为当前时间
        }
    }

}

/*
CREATE TABLE `db_userinfo` (
  `Id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键ID',
  `Username` varchar(255) NOT NULL COMMENT '用户名',
  `PasswordHash` varchar(255) NOT NULL COMMENT '用户密码Hash',
  `Email` varchar(255) NOT NULL COMMENT '用户邮箱',
  `PhoneNumber` varchar(255) NOT NULL COMMENT '用户电话号码',
  `IsActive` tinyint(1) NOT NULL COMMENT '用户活跃状态',
  `IsDeleted` tinyint(1) NOT NULL COMMENT '软删除标志',
  `CreatedTime` datetime NOT NULL COMMENT '创建时间',
  `ModifiedTime` datetime DEFAULT NULL COMMENT '修改时间',
  `CreatorId` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '创建者ID',
  `LastModifierId` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '最后修改者ID',
  `DeleterId` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '删除者ID',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户信息表';
 */