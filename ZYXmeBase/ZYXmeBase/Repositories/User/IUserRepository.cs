﻿using ZYXmeBase.Models;

namespace ZYXmeBase.Repositories.User
{
    public interface IUserRepository
    {
        IEnumerable<ApplicationUser> GetUsers();
        // 可添加更多方法，例如：GetUserById、AddUser、UpdateUser、DeleteUser等
    }
}
