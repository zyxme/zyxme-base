﻿using System.Linq.Expressions;

namespace ZYXmeBase.Repositories
{
    public interface IRepository<TEntity, TKey> where TEntity : class
    {
        Task Add(TEntity entity);

        Task Delete(TEntity entity);

        IQueryable<TEntity> GetAll();

        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        Task<List<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate);



        Task<TEntity> GetById(TKey id);

        Task<int> SaveChangesAsync();

        Task Update(TEntity entity);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);
    }

}
