﻿using Microsoft.AspNetCore.Identity;

namespace ZYXmeBase.Models
{
    public class ApplicationUser : IdentityUser
    {
        // 可在此添加更多属性，例如：
        // public string FullName { get; set; }
    }
}
