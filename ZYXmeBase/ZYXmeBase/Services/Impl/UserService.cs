﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ZYXmeBase.Contracts;
using ZYXmeBase.Dtos;
using ZYXmeBase.Entity.User;
using ZYXmeBase.Repositories;
using ZYXmeBase.Repositories.User;

namespace ZYXmeBase.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRepository<UserInfo, string> _repository;
        private readonly IMapper _mapper;

        public UserService(
            IUserRepository userRepository,
             IRepository<UserInfo, string> repository,
             IMapper mapper
            )
        {
            _userRepository = userRepository;
            _repository = repository; 
            _mapper = mapper;
        }

        public Task<UserInfoDto> GetUserById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UserInfoDto>> GetUserList()
        {
            //var users = _repository.GetAll();
            try
            {
                var uuu = await _repository.GetAll().ToListAsync();
            }
            catch (BusinessException)
            {

                throw new BusinessException("错误啦") ;
            }
            var users = new List<UserInfoDto> { new UserInfoDto { Id = "Id", UserName = "UserName" } };
            return users.Select(u => new UserInfoDto { Id = u.Id, UserName = u.UserName });
            // 应添加错误处理代码
        }

        public async Task<UserInfoDto> Verify(string userName, string password)
        {
            var list = await _repository.GetAll().ToListAsync();
            var entity = await _repository.FirstOrDefaultAsync(x => x.Username == userName && x.PasswordHash == password);
            if (entity == null)
            {
                return null;
            }
            var result = _mapper.Map<UserInfo, UserInfoDto>(entity);
            return result;
        }
    }
}
