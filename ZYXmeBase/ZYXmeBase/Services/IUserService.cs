﻿using ZYXmeBase.Dtos;

namespace ZYXmeBase.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserInfoDto>> GetUserList();

        Task<UserInfoDto> GetUserById(int id);

        Task<UserInfoDto> Verify(string userName,string password);
    }
}
