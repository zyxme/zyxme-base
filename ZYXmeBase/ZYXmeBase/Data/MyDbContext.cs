﻿using Microsoft.EntityFrameworkCore;
using ZYXmeBase.Entity.User;

namespace ZYXmeBase.Data
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
        }
        public DbSet<UserInfo> UserInfo { get; set; }





        // 其他模型和数据库操作
        public override int SaveChanges()
        {
            UpdateAuditFields();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditFields();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateAuditFields()
        {
            // ... 这里把你要自动更新的字段进行处理...
        }
    }
}
