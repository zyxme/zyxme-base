﻿namespace ZYXmeBase.Middleware
{
    public class MyAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public MyAuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            // 检查是否已通过认证
            if (!httpContext.User.Identity.IsAuthenticated)
            {
                // 如果未通过认证，返回 401 Unauthorized 状态码
                httpContext.Response.StatusCode = 401; // Unauthorized
                await httpContext.Response.WriteAsync("Unauthorized");
                return;
            }

            // 通过认证的请求将由后续的中间件处理
            await _next(httpContext);
        }
    }

}
