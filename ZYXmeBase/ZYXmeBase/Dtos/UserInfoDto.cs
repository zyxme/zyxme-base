﻿namespace ZYXmeBase.Dtos
{
    public class UserInfoDto
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        // 可添加更多属性，例如：FullName、Email等
    }
}
