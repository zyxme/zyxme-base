﻿using System.Security.Cryptography;
using System.Text;

namespace ZYXmeBase.Contracts
{
    public class MD5Encrypt
    {
        public static string GetMD5(string input)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);
                return Convert.ToBase64String(hashBytes);
            }
        }
    }
}
