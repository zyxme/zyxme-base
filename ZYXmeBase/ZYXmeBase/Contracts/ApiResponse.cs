﻿using ZYXmeBase.Contracts.Enum;

namespace ZYXmeBase.Contracts
{
    /// <summary>
    /// API返回类型
    /// </summary>
    public class ApiResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        public ApiResponseCode StatusCode { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 成功
        /// </summary>
        public bool IsSuccess => StatusCode == ApiResponseCode.Success;

        /// <summary>
        /// 时间戳(毫秒)
        /// </summary>
        public long Timestamp { get; } = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        /// <summary>
        /// 响应成功
        /// </summary>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ApiResponse IsSuccessd(string message = "")
        {
            Message = message;
            StatusCode = ApiResponseCode.Success;
            return this;
        }

        /// <summary>
        /// 响应失败
        /// </summary>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ApiResponse IsFailed(string message = "")
        {
            Message = message;
            StatusCode = ApiResponseCode.Failed;
            return this;
        }

        /// <summary>
        /// 响应失败
        /// </summary>
        /// <param name="exexception></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ApiResponse IsFailed(Exception exception)
        {
            Message = exception.InnerException?.StackTrace;
            StatusCode = ApiResponseCode.Failed;
            return this;
        }
    }

    /// <summary>
    /// 带泛型值的返回
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResponse<T> : ApiResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        public T Result { get; set; }

        /// <summary>
        /// 响应成功
        /// </summary>
        /// <param name="result"></param>
        /// <param name="message"></param>
        public ApiResponse<T> IsSuccessd(T result, string message = "")
        {
            Message = message;
            StatusCode = ApiResponseCode.Success;
            Result = result;
            return this;
        }

        /// <summary>
        /// 响应失败
        /// </summary>
        /// <param name="result"></param>
        /// <param name="message"></param>
        public ApiResponse<T> IsFailed(T result, string message = "")
        {
            Message = message;
            StatusCode = ApiResponseCode.Failed;
            Result = result;
            return this;
        }
    }
}
