﻿namespace ZYXmeBase.Contracts.Enum
{
    /// <summary>
    /// 返回的StatusCode枚举
    /// </summary>
    public enum ApiResponseCode
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success = 0,

        /// <summary>
        /// 失败
        /// </summary>
        Failed = 1,
    }
}
