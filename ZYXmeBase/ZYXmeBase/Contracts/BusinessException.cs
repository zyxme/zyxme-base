﻿namespace ZYXmeBase.Contracts
{
    public class BusinessException : Exception
    {
        public BusinessException(string message, int statusCode = 200)
        {
            Message = message;
            StatusCode = statusCode;
        }

        public string Message { get; set; }

        public int StatusCode { get; set; }
    }
}
