﻿namespace ZYXmeBase.Contracts.AutoMapperContracts
{
    using AutoMapper;
    using ZYXmeBase.Dtos;
    using ZYXmeBase.Entity.User;

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // 定义映射规则
            CreateMap<UserInfo, UserInfoDto>();
            // 其他映射规则...
        }
    }
}
