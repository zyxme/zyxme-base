﻿using Microsoft.AspNetCore.Mvc;
using ZYXmeBase.Contracts;
using ZYXmeBase.Dtos;
using ZYXmeBase.Services;

namespace ZYXmeBase.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(
            IUserService userService
            )
        {
            _userService = userService;
        }

        [HttpGet("GetUser")]
        public async Task<ApiResponse<UserInfoDto>> GetUser()
        {

            var result = new ApiResponse<UserInfoDto>();
            // 获取用户的整个身份信息
            var user = HttpContext.User;
            // 获取其他声明，例如用户的角色
            String userRole = HttpContext.User.Identity.Name;
            var data = new UserInfoDto { Id = "Id", UserName = userRole };

            var usesss = await _userService.GetUserList();

            result.IsSuccessd(data);
            return result;
        }
    }
}
